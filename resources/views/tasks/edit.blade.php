@extends('layouts.app')
@section('content')



<h1>
   Edit Task:
</h1>

<!-- עריכת משימה -->
<form action="{{action('TaskController@update', $task->id)}}" method='post'>
    @csrf
    @method('PATCH')  

    <div class="form-group">

        <label for="title"> Task to Update: </label>
        <input type="text" class ="form-control" name='title' value= "{{$task->title}}">
    </div>
    <div class="form-group">
        <input type="submit" class ="form-control" name='submit' value="Update">
    </div>

</form>


<!-- עריכת משימה -->
<form action="{{action('TaskController@destroy', $task->id)}}" method='post'>
    @csrf
    @method('Delete')  

    <div class="form-group">

        <label for="title"> Task to Delete: </label>
        <input type="text" class ="form-control" name='title' value= "{{$task->title}}">
    </div>
    <div class="form-group">
        <input type="submit" class ="form-control" name='submit' value="Delete">
    </div>

</form>


@endsection