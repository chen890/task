
@extends('layouts.app')
@section('content')

<h1>This is your tasks list:</h1>
    <ul>
        @foreach($tasks as $task)
        <li>
        id: {{$task->id}} 
        {{$task->title}}
        <a href="{{route('tasks.edit' , $task->id)}}">  Edit    </a>
         @can('admin')
         @method('Delete') 
        <!-- <form action="{{action('TaskController@destroy', $task->id)}}" method='post'> -->

        <a href="{{action('TaskController@destroy', ['id' => $task->id]) }}" > Delete </a>
        @endcan
        </li>
        @endforeach
    </ul>


    <a href="{{route('tasks.create')}}"> Create New Task</a>
    

@endsection