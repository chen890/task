<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

use App\Task;
use App\User;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $task=new Task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->user_id=$id; 
        $task->status=0;            // תיקון שגיאת הסטטוס בהוספה
        $task->save();
        return redirect('tasks');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Gate::denies('admin'))
        {
            abort(403, "No Admin- Get Out!");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);

        return view('tasks.edit', compact('task')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $task = Task::find($id);
        if(!$task ->user->id ==Auth::id())
        {
            return redirect('tasks.index');
        } 
        $task -> update($request->except(['_token']));
        return redirect('tasks'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('admin'))
        {
            abort(403, "No Admin- Get Out!");
        }
        $task = Task::find($id);
        if(!$task ->user->id ==Auth::id())   // משתמש לא רשום
        {
            return redirect('tasks');
        } 
       
        $task->delete();  
        return redirect('tasks');
    }
}
