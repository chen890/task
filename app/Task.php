<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;

class Task extends Model
{
    protected $fillable = [
        'title', 'status'
    ];
    
    public function user()
    {
        // החזר
        return ($this->belongsTo('App\User'));
    } 
}
